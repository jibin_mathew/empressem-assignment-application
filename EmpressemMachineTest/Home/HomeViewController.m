//
//  HomeViewController.m
//  EmpressemMachineTest
//
//  Created by JIBIN MATHEW on 6/10/17.
//  Copyright © 2017 JIBIN MATHEW. All rights reserved.
//

#import "HomeViewController.h"
#import "AlbumViewController.h"
#import <Photos/Photos.h>

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self RequestPermissionToAcessGalary];
    self.navigationItem.title= @"Home";
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Clicks Handling


- (IBAction)AppButtonClicked:(id)sender {
    UIImagePickerController *cardPicker = [[UIImagePickerController alloc]init];
    cardPicker.allowsEditing=YES;
    cardPicker.delegate=self;
    
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) { // check to redirect in stimulator
        cardPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else{
        cardPicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    [self presentViewController:cardPicker animated:YES
                     completion:^{}];
}

- (IBAction)AlbumButtonClicked:(id)sender {
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main"
                                                 bundle:nil];
    UIViewController *albumVC = [sb instantiateViewControllerWithIdentifier:@"AlbumViewController"];
    [self.navigationController pushViewController:albumVC
                                         animated:YES];
}

- (IBAction)InfoButtonClicked:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main"
                                                 bundle:nil];
    UIViewController *infoVC = [sb instantiateViewControllerWithIdentifier:@"MyInfoViewController"];
    [self.navigationController pushViewController:infoVC
                                         animated:YES];
}
#pragma mark - Image Picker handling

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage  *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSString *mediaType = [info objectForKey:@"mediaType"];
    
    if ([mediaType isEqualToString:kCIAttributeTypeImage]){
        
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        
    }else{
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *sourcePath = [documentsDirectory stringByAppendingPathComponent:@"video.mp4"];
        UISaveVideoAtPathToSavedPhotosAlbum(sourcePath,nil,nil,nil);
        
    }
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        
    }];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker

{
    // Dismiss the image selection and close the program
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}
-(void)RequestPermissionToAcessGalary{
    
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    
    if (status != PHAuthorizationStatusAuthorized) {
        
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
        }];
    }
    
}
@end
