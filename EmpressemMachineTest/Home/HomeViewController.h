//
//  HomeViewController.h
//  EmpressemMachineTest
//
//  Created by JIBIN MATHEW on 6/10/17.
//  Copyright © 2017 JIBIN MATHEW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>


- (IBAction)AppButtonClicked:(id)sender;
- (IBAction)AlbumButtonClicked:(id)sender;
- (IBAction)InfoButtonClicked:(id)sender;

@end
