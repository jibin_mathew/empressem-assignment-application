//
//  RequestManager.m
//  EmpressemMachineTest
//
//  Created by JIBIN MATHEW on 6/10/17.
//  Copyright © 2017 JIBIN MATHEW. All rights reserved.
//

#import "RequestManager.h"

# define BaseUrlForSignUp @"https://recordio.empressemdemo.com/register/signupapi"
# define BaseUrlForSignIn @"https://recordio.empressemdemo.com/register/login"


@implementation RequestManager


+ (void)UpdateSignupRequestWithName:(NSString*)name UserName:(NSString*)userName email:(NSString*)email password :(NSString*)password andCompletionHandler:(CompletionBlock)completion {
    
    NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@?name=%@&username=%@&email=%@&password=%@",BaseUrlForSignUp,name,userName,email,password];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data,NSURLResponse *response,NSError *error){
        if(data != nil){
            completion(true);
        }else{
            completion(false);
            NSLog(@"Sign Up Register Error ::: %@",error.description);
        }
    }]resume];
}

+ (void)SendLoginRequestWithEmail:(NSString*)email  password :(NSString*)password andCompletionHandler:(CompletionBlock)completion {
    
    NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@?email=%@&password=%@",BaseUrlForSignIn,email,password];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data,NSURLResponse *response,NSError *error){
        if(data != nil){
            completion(true);
        }else{
            completion(false);
            NSLog(@"Sign In Register Error ::: %@",error.description);
        }
    }]resume];
}
@end
