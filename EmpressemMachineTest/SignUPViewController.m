//
//  ViewController.m
//  EmpressemMachineTest
//
//  Created by JIBIN MATHEW on 6/10/17.
//  Copyright © 2017 JIBIN MATHEW. All rights reserved.
//

#import "SignUPViewController.h"
#import "RequestManager.h"
#import "LoginViewController.h"
#import "MBProgressHUD.h"

@interface SignUPViewController ()

@end

@implementation SignUPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title= @"Sign Up";
    
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - sign up Button Handling


- (IBAction)signUpButtonClicked:(id)sender {
    
    if (_nameTextField.text.length == 0 || _userNameTextField.text.length == 0 ||_emailTextField.text.length == 0 || _passwordTextField.text.length == 0) {
        [self ShowAlertWithTitle:@"All fields are mandatory"];
    }
    else{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [RequestManager UpdateSignupRequestWithName:_nameTextField.text
                                           UserName:_userNameTextField.text
                                              email:_emailTextField.text
                                           password:_passwordTextField.text andCompletionHandler:^(BOOL status) {
                                               if (status == true) {
                                                   ///Move to Login screen
                                               }else{
                                                   // show alert
                                               }
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   [self MoveTologinScreen];// As url request always fails So choose to move in all conditions
                                                   [MBProgressHUD hideHUDForView:self.view animated:YES];
                                               });
                                           }];
    }
    
    
}

-(void)ShowAlertWithTitle:(NSString*)message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Title" message: message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Moving to Next Screen

-(void)MoveTologinScreen{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *loginVC = [sb instantiateViewControllerWithIdentifier:@"LoginViewController"];
    
    [self.navigationController pushViewController:loginVC animated:YES];
}
@end
