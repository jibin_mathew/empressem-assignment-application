//
//  AppDelegate.h
//  EmpressemMachineTest
//
//  Created by JIBIN MATHEW on 6/10/17.
//  Copyright © 2017 JIBIN MATHEW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

