//
//  UserManager.m
//  EmpressemMachineTest
//
//  Created by JIBIN MATHEW on 6/10/17.
//  Copyright © 2017 JIBIN MATHEW. All rights reserved.
//

#import "UserManager.h"



@implementation UserManager




+ (UserManager *)sharedInstance {
    
    static dispatch_once_t onceToken;
    static id sharedManager = nil;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

-(void)saveUserDetails:(NSString*)username andEmail:(NSString*)email Name: (NSString*)name{
    
    self.name            = name;
    self.email           = email;
    self.username        = username;
}





@end




