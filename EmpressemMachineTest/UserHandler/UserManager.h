//
//  UserManager.h
//  EmpressemMachineTest
//
//  Created by JIBIN MATHEW on 6/10/17.
//  Copyright © 2017 JIBIN MATHEW. All rights reserved.
//

#import <Foundation/Foundation.h>
@class User;



@interface UserManager : NSObject{
    
}




@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *email;


+ (UserManager *)sharedInstance;
-(void)saveUserDetails:(NSString*)username andEmail:(NSString*)email Name: (NSString*)name;


@end



