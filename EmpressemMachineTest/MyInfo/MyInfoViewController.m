//
//  MyInfoViewController.m
//  EmpressemMachineTest
//
//  Created by JIBIN MATHEW on 6/10/17.
//  Copyright © 2017 JIBIN MATHEW. All rights reserved.
//

#import "MyInfoViewController.h"
#import "UserManager.h"

@interface MyInfoViewController ()

@end

@implementation MyInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ShowUserDeatils];
    self.navigationItem.title=@"My Info";
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Adding details to View

-(void)ShowUserDeatils{
    UserManager *userData = [UserManager sharedInstance];
    
    if(userData.email) {
        self.emailLabel.text = [NSString stringWithFormat:@"E-mail :%@",userData.email];
    }
    if(userData.name) {
        self.namelabel.text = [NSString stringWithFormat:@"Name :%@",userData.name];
    }
    if(userData.username) {
        self.userNameLabel.text = [NSString stringWithFormat:@"UserName :%@",userData.username];;
    }
}

@end
