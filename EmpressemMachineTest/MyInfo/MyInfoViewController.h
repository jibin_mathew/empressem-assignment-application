//
//  MyInfoViewController.h
//  EmpressemMachineTest
//
//  Created by JIBIN MATHEW on 6/10/17.
//  Copyright © 2017 JIBIN MATHEW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyInfoViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *namelabel;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@end
