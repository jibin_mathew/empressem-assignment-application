//
//  AlbumViewController.h
//  EmpressemMachineTest
//
//  Created by JIBIN MATHEW on 6/10/17.
//  Copyright © 2017 JIBIN MATHEW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>





@property (weak, nonatomic) IBOutlet UICollectionView *albumCollectionView;

@end
