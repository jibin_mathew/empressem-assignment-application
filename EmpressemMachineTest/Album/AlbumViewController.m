//
//  AlbumViewController.m
//  EmpressemMachineTest
//
//  Created by JIBIN MATHEW on 6/10/17.
//  Copyright © 2017 JIBIN MATHEW. All rights reserved.
//

#import "AlbumViewController.h"
#import <Photos/Photos.h>
#import "AlbumCell.h"

@interface AlbumViewController (){
    NSMutableArray *assets ;
}
@property(strong,nonatomic)PHImageRequestOptions *requestOptions;
@end


#define CELL_WIDHT 100
#define CELL_HEIGHT 100
#define CELL_PADDING 15
#define MAX_CELL_COUNT 50
#define CELL_IDENTIFIER @"albumCell"

@implementation AlbumViewController

@synthesize albumCollectionView;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fetchPhotoLibraryImages];
    self.navigationItem.title=@"My Album";
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Photo Library handling

-(void)fetchPhotoLibraryImages{
    
    self.requestOptions = [[PHImageRequestOptions alloc] init];
    self.requestOptions.resizeMode   = PHImageRequestOptionsResizeModeExact;
    self.requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
    
    
    self.requestOptions.synchronous = true;
    PHFetchResult *results = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:nil];
    assets = [NSMutableArray arrayWithCapacity:results.count];
    
    [results enumerateObjectsUsingBlock:^(PHAsset *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [assets addObject:obj];
    }];
    [self InitializeCollectionView];
    
}
#pragma mark - Collectionview handling
-(void)InitializeCollectionView{
    
    [albumCollectionView setDataSource:self];
    [albumCollectionView setDelegate:self];
    [albumCollectionView registerNib:[UINib nibWithNibName:@"AlbumCell" bundle:nil] forCellWithReuseIdentifier:CELL_IDENTIFIER];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(assets.count >= MAX_CELL_COUNT){
        return MAX_CELL_COUNT;
    }
    return assets.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AlbumCell *cell=(AlbumCell*)[collectionView dequeueReusableCellWithReuseIdentifier:CELL_IDENTIFIER forIndexPath:indexPath];
    if (assets[indexPath.row] != nil) {
        [self getImageFromAsset:assets[indexPath.row] andImageView:cell.imageView andIsThumNail:YES];
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CELL_WIDHT, CELL_HEIGHT);
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(50, CELL_PADDING,CELL_PADDING,CELL_PADDING);
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (assets[indexPath.row] != nil)
        [self addImageViewWithImage:assets[indexPath.row]];
}

#pragma mark - ImageView handling

-(void)addImageViewWithImage:(PHAsset*)asset {
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [self getImageFromAsset:asset andImageView:imgView andIsThumNail:false];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    imgView.userInteractionEnabled=true;
    imgView.tag = 100;
    UITapGestureRecognizer *dismissTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeImage)];
    dismissTap.numberOfTapsRequired = 1;
    self.requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    [imgView addGestureRecognizer:dismissTap];
    imgView.backgroundColor=[UIColor blackColor];
    [self.view addSubview:imgView];
    
}
-(void)removeImage {
    
    UIImageView *imgView = (UIImageView*)[self.view viewWithTag:100];
    [imgView removeFromSuperview];
    
}
-(void)getImageFromAsset:(PHAsset*)asset andImageView:(UIImageView*)imageView andIsThumNail:(BOOL)isthumnail{
    
    CGSize size;
    PHImageManager *manager = [PHImageManager defaultManager];
    
    if (isthumnail == YES) {
        size=CGSizeMake(CELL_WIDHT, CELL_HEIGHT);
    }else{
        size= CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height);
    }
    
    [manager requestImageForAsset:asset
                       targetSize:size
                      contentMode:PHImageContentModeAspectFit
                          options:self.requestOptions
                    resultHandler:^void(UIImage *image, NSDictionary *info) {
                        imageView.image=image;
                        imageView.backgroundColor =[UIColor redColor];
                    }];
}
@end
