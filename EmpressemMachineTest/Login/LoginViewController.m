//
//  LoginViewController.m
//  EmpressemMachineTest
//
//  Created by JIBIN MATHEW on 6/10/17.
//  Copyright © 2017 JIBIN MATHEW. All rights reserved.
//

#import "LoginViewController.h"
#import "RequestManager.h"
#import "UserManager.h"
#import "MBProgressHUD.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title =   @"Login";
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Login Button handling

- (IBAction)LoginButtonClicked:(id)sender {
    
    if (_emailTextField.text.length == 0 ||_passwordTextField.text.length ==0) {
        [self ShowAlertWithTitle:@"All fields are mandatory"];
    }
    else{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        /////// sending Signup request //////////
        [RequestManager SendLoginRequestWithEmail:_emailTextField.text
                                         password:_passwordTextField.text
                             andCompletionHandler:^(BOOL status) {
                                 
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     [self MoveToHomeScreen];
                                     /// Always Getting error in request so anyway saving it
                                     [self SaveCreditials];
                                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                                 });
                             }];
        
    }
    
}
#pragma mark - Saving
-(void)SaveCreditials{
    
    [self SaveLoginCredialsLocally];
    //// No need but saved in User defaults /////////
    [[NSUserDefaults standardUserDefaults] setValue:_emailTextField.text
                                             forKey:@"Username"];
    [[NSUserDefaults standardUserDefaults] setValue:_passwordTextField.text
                                             forKey:@"password"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
}
-(void)SaveLoginCredialsLocally{
    ///saving to singleton class ///////
    [[UserManager sharedInstance]saveUserDetails:@"John12"
                                        andEmail:_emailTextField.text
                                            Name:@"John"];
}
-(void)ShowAlertWithTitle:(NSString*)message{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Title"
                                                                             message: message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK"
                                                 style:UIAlertActionStyleDefault
                                               handler:nil];
    [alertController addAction:ok];
    [self presentViewController:alertController
                       animated:YES
                     completion:nil];
}

#pragma mark - Navigation to Next Screen
-(void)MoveToHomeScreen{
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main"
                                                 bundle:nil];
    UIViewController *homeVc = [sb instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [self.navigationController pushViewController:homeVc
                                         animated:YES];
}
@end
