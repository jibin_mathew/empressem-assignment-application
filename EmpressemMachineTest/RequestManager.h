//
//  RequestManager.h
//  EmpressemMachineTest
//
//  Created by JIBIN MATHEW on 6/10/17.
//  Copyright © 2017 JIBIN MATHEW. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestManager : NSObject
typedef void (^CompletionBlock)(BOOL status);





+ (void)UpdateSignupRequestWithName:(NSString*)name UserName:(NSString*)userName email:(NSString*)email password :(NSString*)password andCompletionHandler:(CompletionBlock)completion;
    
+ (void)SendLoginRequestWithEmail:(NSString*)email  password :(NSString*)password andCompletionHandler:(CompletionBlock)completion;
@end
